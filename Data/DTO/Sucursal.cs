﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class Sucursal
    {
        public string? IdEmpresa { get; set; }
        public string? IdSucursal { get; set; }
        public string? Nombre { get; set; }
        public string? Direccion { get; set; }
        public bool? Activa { get; set; }
        public string? Tipo { get; set; }
    }
}
