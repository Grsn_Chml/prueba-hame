﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class Producto
    {
        public string? IdProducto { get; set; }
        public string? IdEmpresa { get; set; }
        public string? IdMaestro { get; set; }
        public string? IdPosicion { get; set; }
        public string? IdInventario { get; set; }
        public string? IdKardex { get; set; }
        public string? IdSucursal { get; set; }
        public string? Sucursal { get; set; }
        public int? Numero { get; set; }
        public string? Codigo { get; set; }
        public string? CodigoFabricante { get; set; }
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
        public string? Sku { get; set; }
        public string? Unidad { get; set; }
        public int? Unidades { get; set; }
        public decimal? Costo { get; set; }
        public int? Precio { get; set; }
        public decimal? Precio1 { get; set; }
        public decimal? Precio2 { get; set; }
        public decimal? Precio3 { get; set; }
        public decimal? Precio4 { get; set; }
        public decimal? Precio5 { get; set; }
        public string? Tipo { get; set; }   
        public string? Imagen { get; set; }
        public bool? Habilitado { get; set; }
        public decimal? Existencia { get; set; } 
        public string? Posicion { get; set; }
        public int? Minimo { get; set; }
        public int? Maximo { get; set; }
    }
}
