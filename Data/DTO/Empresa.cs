﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class Empresa
    {
        public string IdEmpresa { get; set; } = null!;
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
    }
}
