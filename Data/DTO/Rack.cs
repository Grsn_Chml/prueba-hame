﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class Rack
    {
        public string? IdRack { get; set; }
        public string? IdSucursal { get; set; }
        public int? Nivel { get; set; }
        public string? Posicion { get; set; }
        public string? Zona { get; set; }
        public bool? Activo { get; set; }
        public string? Observacion { get; set; }
        public string? Lado { get; set; }
        public string? Pasillo { get; set; }
    }
}
