﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class Posicion
    {
        public string? IdRack { get; set; }
        public string? IdPosicion { get; set; }
        public string? Rack { get; set; }
        public string? Sucursal { get; set; }
        public string? Codigo { get; set; }
        public int? Nivel { get; set; }
        public int? Posicion1 { get; set; }
        public int? Fondo { get; set; }
        public bool? Activo { get; set; }
        public string? Zona { get; set; }
        public string? Observaciones { get; set; }
    }
}
