﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Posicion
    {
        public Posicion()
        {
            Inventarios = new HashSet<Inventario>();
        }

        public string? IdRack { get; set; }
        public string IdPosicion { get; set; } = null!;
        public string? Codigo { get; set; }
        public int? Nivel { get; set; }
        public int? Posicion1 { get; set; }
        public int? Fondo { get; set; }
        public bool? Activo { get; set; }
        public string? Zona { get; set; }
        public string? Observaciones { get; set; }

        public virtual Rack? Rack { get; set; }
        public virtual ICollection<Inventario> Inventarios { get; set; }
    }
}
