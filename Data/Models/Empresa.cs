﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Empresa
    {
        public Empresa()
        {
            Maestros = new HashSet<Maestro>();
            Productos = new HashSet<Producto>();
            Sucursals = new HashSet<Sucursal>();
        }

        public string IdEmpresa { get; set; } = null!;
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
        public DateTime? Creada { get; set; }
        public DateTime? Modificada { get; set; }
        public bool? Activa { get; set; }

        public virtual ICollection<Maestro> Maestros { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
        public virtual ICollection<Sucursal> Sucursals { get; set; }
    }
}
