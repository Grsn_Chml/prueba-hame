﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Producto
    {
        public Producto()
        {
            Kardices = new HashSet<Kardex>();
        }

        public string IdProducto { get; set; } = null!;
        public string? IdEmpresa { get; set; }
        public string? IdMaestro { get; set; }
        public int Numero { get; set; }
        public string? Codigo { get; set; }
        public string? CodigoFabricante { get; set; }
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
        public string? Sku { get; set; }
        public string? Unidad { get; set; }
        public int? Unidades { get; set; }
        public decimal? Costo { get; set; }
        public int? Precio { get; set; }
        public decimal? Precio1 { get; set; }
        public decimal? Precio2 { get; set; }
        public decimal? Precio3 { get; set; }
        public decimal? Precio4 { get; set; }
        public decimal? Precio5 { get; set; }
        public string? Imagen { get; set; }
        public bool? Habilitado { get; set; }

        public virtual Empresa? IdEmpresaNavigation { get; set; }
        public virtual Maestro? IdMaestroNavigation { get; set; }
        public virtual ICollection<Kardex> Kardices { get; set; }
    }
}
