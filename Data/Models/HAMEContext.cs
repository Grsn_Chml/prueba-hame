﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Data.Models
{
    public partial class HAMEContext : DbContext
    {
        public HAMEContext()
        {
        }

        public HAMEContext(DbContextOptions<HAMEContext> options) : base(options) { }

        public virtual DbSet<Empresa> Empresas { get; set; } = null!;
        public virtual DbSet<Inventario> Inventarios { get; set; } = null!;
        public virtual DbSet<Kardex> Kardexs { get; set; } = null!;
        public virtual DbSet<Maestro> Maestros { get; set; } = null!;
        public virtual DbSet<Posicion> Posicions { get; set; } = null!;
        public virtual DbSet<Producto> Productos { get; set; } = null!;
        public virtual DbSet<Rack> Racks { get; set; } = null!;
        public virtual DbSet<Sucursal> Sucursals { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empresa>(entity =>
            {
                entity.HasKey(e => e.IdEmpresa)
                    .HasName("PK__Empresa__5EF4033E10A13257");

                entity.ToTable("Empresa");

                entity.Property(e => e.IdEmpresa)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Creada).HasColumnType("datetime");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Modificada).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Inventario>(entity =>
            {
                entity.HasKey(e => e.IdInventario)
                    .HasName("PK__Inventar__1927B20CB8E15114");

                entity.ToTable("Inventario");

                entity.Property(e => e.IdInventario)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Existencia).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.IdMaestro)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.IdPosicion)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdMaestroNavigation)
                    .WithMany(p => p.Inventarios)
                    .HasForeignKey(d => d.IdMaestro)
                    .HasConstraintName("FK_IdMaestroInventario");

                entity.HasOne(d => d.IdPosicionNavigation)
                    .WithMany(p => p.Inventarios)
                    .HasForeignKey(d => d.IdPosicion)
                    .HasConstraintName("FK_IdPosicionInventario");
            });

            modelBuilder.Entity<Kardex>(entity =>
            {
                entity.HasKey(e => e.IdKardex)
                    .HasName("PK__Kardex__BC1BA400E89B935B");

                entity.ToTable("Kardex");

                entity.Property(e => e.IdKardex)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Accion)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Cantidad).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Codigo)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Existencia).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.IdCompra)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.IdInventario)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.IdProducto)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.IdTraslado)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.IdVenta)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Motivo)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Sku)
                    .HasMaxLength(75)
                    .IsUnicode(false)
                    .HasColumnName("SKU");

                entity.HasOne(d => d.IdInventarioNavigation)
                    .WithMany(p => p.Kardices)
                    .HasForeignKey(d => d.IdInventario)
                    .HasConstraintName("FK_IdInventarioKardex");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.Kardices)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("FK_IdProductoKardex");
            });

            modelBuilder.Entity<Maestro>(entity =>
            {
                entity.HasKey(e => e.IdMaestro)
                    .HasName("PK__Maestro__66B8F189378E1650");

                entity.ToTable("Maestro");

                entity.Property(e => e.IdMaestro)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Codigo)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoFabricante)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Costo).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IdEmpresa)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Imagen)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Numero).ValueGeneratedOnAdd();

                entity.Property(e => e.Sku)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("SKU");

                entity.Property(e => e.Tipo)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Maestros)
                    .HasForeignKey(d => d.IdEmpresa)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_IdEmpresaMaestro");
            });

            modelBuilder.Entity<Posicion>(entity =>
            {
                entity.HasKey(e => e.IdPosicion)
                    .HasName("PK__Posicion__638A2F4C3F96A106");

                entity.ToTable("Posicion");

                entity.Property(e => e.IdPosicion)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Codigo)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.IdRack)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Posicion1).HasColumnName("Posicion");

                entity.Property(e => e.Zona)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.Rack)
                    .WithMany(p => p.Posicions)
                    .HasForeignKey(d => d.IdRack)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_IdRack");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.IdProducto)
                    .HasName("PK__Producto__09889210D9B4F3DC");

                entity.ToTable("Producto");

                entity.Property(e => e.IdProducto)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Codigo)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoFabricante)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Costo).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IdEmpresa)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.IdMaestro)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Imagen)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Numero).ValueGeneratedOnAdd();

                entity.Property(e => e.Precio1).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Precio2).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Precio3).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Precio4).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Precio5).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Sku)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("SKU");

                entity.Property(e => e.Unidad)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdEmpresa)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_IdEmpresaProducto");

                entity.HasOne(d => d.IdMaestroNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdMaestro)
                    .HasConstraintName("FK_IdMaestroProducto");
            });

            modelBuilder.Entity<Rack>(entity =>
            {
                entity.HasKey(e => e.IdRack)
                    .HasName("PK__Rack__8F06D0055CDDE050");

                entity.ToTable("Rack");

                entity.Property(e => e.IdRack)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.IdSucursal)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Lado)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Observacion)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Pasillo)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Posicion)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Zona)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.HasOne(d => d.Sucursal)
                    .WithMany(p => p.Racks)
                    .HasForeignKey(d => d.IdSucursal)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_IdSucursal");
            });

            modelBuilder.Entity<Sucursal>(entity =>
            {
                entity.HasKey(e => e.IdSucursal)
                    .HasName("PK__Sucursal__BFB6CD99DE2527D9");

                entity.ToTable("Sucursal");

                entity.Property(e => e.IdSucursal)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Creada).HasColumnType("datetime");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IdEmpresa)
                    .HasMaxLength(75)
                    .IsUnicode(false);

                entity.Property(e => e.Modificada).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Tipo)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdEmpresaNavigation)
                    .WithMany(p => p.Sucursals)
                    .HasForeignKey(d => d.IdEmpresa)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_IdEmpresa");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
