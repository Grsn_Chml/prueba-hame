﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Inventario
    {
        public Inventario()
        {
            Kardices = new HashSet<Kardex>();
        }

        public string IdInventario { get; set; } = null!;
        public string? IdPosicion { get; set; }
        public string? IdMaestro { get; set; }
        public decimal? Existencia { get; set; }
        public int? Minimo { get; set; }
        public int? Maximo { get; set; }
        public DateTime? Fecha { get; set; }

        public virtual Maestro? IdMaestroNavigation { get; set; }
        public virtual Posicion? IdPosicionNavigation { get; set; }
        public virtual ICollection<Kardex> Kardices { get; set; }
    }
}
