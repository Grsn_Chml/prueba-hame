﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Maestro
    {
        public Maestro()
        {
            Inventarios = new HashSet<Inventario>();
            Productos = new HashSet<Producto>();
        }

        public string IdMaestro { get; set; } = null!;
        public string? IdEmpresa { get; set; }
        public int Numero { get; set; }
        public string? Codigo { get; set; }
        public string? CodigoFabricante { get; set; }
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
        public string? Tipo { get; set; }
        public decimal? Costo { get; set; }
        public int? Minimo { get; set; }
        public int? Maximo { get; set; }
        public string? Imagen { get; set; }
        public bool? Habilitado { get; set; }
        public string? Sku { get; set; }

        public virtual Empresa? IdEmpresaNavigation { get; set; }
        public virtual ICollection<Inventario> Inventarios { get; set; }
        public virtual ICollection<Producto> Productos { get; set; }
    }
}
