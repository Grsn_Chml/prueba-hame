﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Sucursal
    {
        public Sucursal()
        {
            Racks = new HashSet<Rack>();
        }

        public string? IdEmpresa { get; set; }
        public string IdSucursal { get; set; } = null!;
        public string? Nombre { get; set; }
        public string? Direccion { get; set; }
        public DateTime? Creada { get; set; }
        public DateTime? Modificada { get; set; }
        public bool? Activa { get; set; }
        public string? Tipo { get; set; }

        public virtual Empresa? IdEmpresaNavigation { get; set; }
        public virtual ICollection<Rack> Racks { get; set; }
    }
}
