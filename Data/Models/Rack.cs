﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Rack
    {
        public Rack()
        {
            Posicions = new HashSet<Posicion>();
        }

        public string IdRack { get; set; } = null!;
        public string? IdSucursal { get; set; }
        public int? Nivel { get; set; }
        public string? Posicion { get; set; }
        public string? Zona { get; set; }
        public bool? Activo { get; set; }
        public string? Observacion { get; set; }
        public string? Lado { get; set; }
        public string? Pasillo { get; set; }

        public virtual Sucursal? Sucursal { get; set; }
        public virtual ICollection<Posicion> Posicions { get; set; }
    }
}
