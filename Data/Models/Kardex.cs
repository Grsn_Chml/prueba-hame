﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Kardex
    {
        public string IdKardex { get; set; } = null!;
        public string? IdVenta { get; set; }
        public string? IdCompra { get; set; }
        public string? IdTraslado { get; set; }
        public string? IdInventario { get; set; }
        public string? IdProducto { get; set; }
        public decimal? Cantidad { get; set; }
        public decimal? Existencia { get; set; }
        public DateTime? Fecha { get; set; }
        public string? Accion { get; set; }
        public string? Motivo { get; set; }
        public string? Codigo { get; set; }
        public string? Sku { get; set; }
        public string? Nombre { get; set; }

        public virtual Inventario? IdInventarioNavigation { get; set; }
        public virtual Producto? IdProductoNavigation { get; set; }
    }
}
