﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Traslada
    {
        public static Data.DTO.Producto Update(HAMEContext db, Data.DTO.Producto producto)
        {
            try
            {
                producto.IdKardex = Guid.NewGuid().ToString().ToUpper();

                db.Database.ExecuteSqlRaw("EXECUTE UpdateTraslado @IdProducto={0}, @IdEmpresa={1}, @IdMaestro={2}, @IdInventario={3}, @IdPosicion={4}, @Codigo={5}, @Nombre={6}, @SKU={7}, @Existencia={8} ",
                    producto.IdProducto,
                    producto.IdEmpresa,
                    producto.IdMaestro,
                    producto.IdInventario,
                    producto.IdPosicion,
                    producto.Codigo,
                    producto.Nombre,
                    producto.Sku,
                    producto.Existencia);
                return producto;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
