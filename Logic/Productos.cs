﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using static System.Net.Mime.MediaTypeNames;

namespace Logic
{
    public class Productos
    {
        public static List<Data.DTO.Producto> Find(HAMEContext db, string IdProducto, string Nombre, string Descripcion)
        {
            Func<Data.DTO.Producto, bool> func = d => d.Habilitado == true;

            if (!string.IsNullOrWhiteSpace(Nombre))
            {
                func = s => s.Nombre.Contains(Nombre);
            }
               
            if(!string.IsNullOrWhiteSpace(Descripcion))
            {
                func = s => s.Descripcion.Contains(Descripcion);
            }

            if(!string.IsNullOrWhiteSpace(IdProducto))
            {
                func = s => s.IdProducto.Equals(IdProducto);
            }

            var list = (from pr in db.Productos
                        join ir in db.Inventarios on new { i = pr.IdMaestro } equals new { i = ir.IdMaestro }
                        join mr in db.Maestros on new { m = ir.IdMaestro } equals new { m = mr.IdMaestro }
                        join ps in db.Posicions on new { p = ir.IdPosicion } equals new { p = ps.IdPosicion }
                        orderby pr.Numero ascending
                        select new Data.DTO.Producto
                        {
                            Nombre = pr.Nombre,
                            Codigo = pr.Codigo,
                            CodigoFabricante = pr.CodigoFabricante,
                            Costo = pr.Costo,
                            Descripcion = pr.Descripcion,
                            Habilitado = pr.Habilitado,
                            IdEmpresa = pr.IdEmpresa,
                            IdMaestro = pr.IdMaestro,
                            IdProducto = pr.IdProducto,
                            IdInventario = ir.IdInventario,
                            Imagen = pr.Imagen,
                            Numero = pr.Numero,
                            Precio = pr.Precio,
                            Precio1 = pr.Precio1,
                            Precio2 = pr.Precio2,
                            Precio3 = pr.Precio3,
                            Precio4 = pr.Precio4,
                            Precio5 = pr.Precio5,
                            Sku = pr.Sku,
                            Unidad = pr.Unidad,
                            Unidades = pr.Unidades,
                            Existencia = ir.Existencia,
                            Posicion = ps.Codigo,
                            IdPosicion = ps.IdPosicion,
                            Maximo = ir.Maximo,
                            Minimo = ir.Minimo,
                            Tipo = mr.Tipo,
                        });

            return list.Where(func).ToList();
        }

        public static Data.DTO.Producto Create(HAMEContext db, Data.DTO.Producto producto)
        {
            try
            {
                producto.IdProducto = Guid.NewGuid().ToString().ToUpper();
                producto.IdMaestro = Guid.NewGuid().ToString().ToUpper();
                producto.IdInventario = Guid.NewGuid().ToString().ToUpper();
                producto.IdKardex = Guid.NewGuid().ToString().ToUpper();

                db.Database.ExecuteSqlRaw("EXECUTE CreateProducto @IdProducto={0}, @IdEmpresa={1}, @IdMaestro={2}, @IdInventario={3}, @IdPosicion={4}, @IdKardex={5}, @Codigo={6}, @CodigoFabricante={7}, @Nombre={8}, @Descripcion={9}, @SKU={10}, @Unidad={11}, @Unidades={12}, @Costo={13}, @Precio={14}, @Precio1={15}, @Precio2={16}, @Precio3={17}, @Precio4={18}, @Precio5={19}, @Imagen={20}, @Existencia={21}, @Minimo={22}, @Maximo={23}, @Tipo={24} ",
                    producto.IdProducto,
                    producto.IdEmpresa,
                    producto.IdMaestro,
                    producto.IdInventario,
                    producto.IdPosicion,
                    producto.IdKardex,
                    producto.Codigo,
                    producto.CodigoFabricante,
                    producto.Nombre,
                    producto.Descripcion,
                    producto.Sku,
                    producto.Unidad,
                    producto.Unidades,
                    producto.Costo,
                    producto.Precio,
                    producto.Precio1,
                    producto.Precio2,
                    producto.Precio3,
                    producto.Precio4,
                    producto.Precio5,
                    producto.Imagen,
                    producto.Existencia,
                    producto.Minimo,
                    producto.Maximo,
                    producto.Tipo);
                return producto;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public static Data.DTO.Producto Update(HAMEContext db, Data.DTO.Producto producto)
        {
            try
            {
                producto.IdKardex = Guid.NewGuid().ToString().ToUpper();

                db.Database.ExecuteSqlRaw("EXECUTE UpdateProducto @IdProducto={0}, @IdEmpresa={1}, @IdMaestro={2}, @IdInventario={3}, @IdPosicion={4}, @IdKardex={5}, @Codigo={6}, @CodigoFabricante={7}, @Nombre={8}, @Descripcion={9}, @SKU={10}, @Unidad={11}, @Unidades={12}, @Costo={13}, @Precio={14}, @Precio1={15}, @Precio2={16}, @Precio3={17}, @Precio4={18}, @Precio5={19}, @Imagen={20}, @Existencia={21}, @Minimo={22}, @Maximo={23}, @Tipo={24} ",
                    producto.IdProducto,
                    producto.IdEmpresa,
                    producto.IdMaestro,
                    producto.IdInventario,
                    producto.IdPosicion,
                    producto.IdKardex,
                    producto.Codigo,
                    producto.CodigoFabricante,
                    producto.Nombre,
                    producto.Descripcion,
                    producto.Sku,
                    producto.Unidad,
                    producto.Unidades,
                    producto.Costo,
                    producto.Precio,
                    producto.Precio1,
                    producto.Precio2,
                    producto.Precio3,
                    producto.Precio4,
                    producto.Precio5,
                    producto.Imagen,
                    producto.Existencia,
                    producto.Minimo,
                    producto.Maximo,
                    producto.Tipo);
                return producto;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public static string Delete(HAMEContext db, string IdMaestro)
        {
            try
            {
                db.Database.ExecuteSqlRaw("EXECUTE DeleteProducto @IdMaestro={0} ",
                    IdMaestro);
                return "Ok";
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}