﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Posicion
    {
        public static List<Data.DTO.Posicion> Get(HAMEContext db, string IdPosicion, string IdRack)
        {
            Func<Data.DTO.Posicion, bool> func = d => 1 == 1;

            if(!string.IsNullOrEmpty(IdPosicion))
            {
                func = s => s.IdPosicion.Equals(IdPosicion);
            }

            if(!string.IsNullOrEmpty(IdRack))
            {
                func = s => s.IdRack.Equals(IdRack);
            }

            var lista = (from d in db.Posicions
                        select new Data.DTO.Posicion()
                        {
                            Activo = d.Activo,
                            Codigo = d.Codigo,
                            Fondo = d.Fondo,
                            IdPosicion = d.IdPosicion,
                            IdRack = d.IdRack,
                            Nivel = d.Nivel,
                            Observaciones = d.Observaciones,
                            Posicion1 = d.Posicion1,
                            Zona = d.Zona,
                            Sucursal = d.Rack.Sucursal.Nombre,
                            Rack = d.Rack.Nivel.ToString() + "-" + d.Rack.Posicion + "-" + d.Rack.Lado + "-" + d.Rack.Pasillo,
                        });

            return lista.Where(func).ToList();
        }

        public static Data.DTO.Posicion Create(HAMEContext db, Data.DTO.Posicion posicion)
        {
            try
            {
                posicion.IdPosicion = Guid.NewGuid().ToString();

                db.Database.ExecuteSqlRaw("EXECUTE CreatePosicion @IdRack={0}, @IdPosicion={1}, @Codigo={2}, @Nivel={3}, @Posicion={4}, @Fondo={5}, @Zona={6}, @Observaciones={7} ",
                    posicion.IdRack,
                    posicion.IdPosicion,
                    posicion.Codigo,
                    posicion.Nivel,
                    posicion.Posicion1,
                    posicion.Fondo,
                    posicion.Zona,
                    posicion.Observaciones);

                return posicion;
            }
            catch 
            {
                return null;
            }
        }
    }
}
