﻿using Data.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Rack
    {
        public static List<Data.DTO.Rack> Find(HAMEContext db, string IdRack, string IdSucursal)
        {
            Func<Data.DTO.Rack, bool> func = d => 1 == 1;

            if (!string.IsNullOrEmpty(IdRack))
            {
                func = s => s.IdRack.Equals(IdRack);
            }

            if(!string.IsNullOrEmpty(IdSucursal))
            {
                func = s => s.IdSucursal.Equals(IdSucursal);
            }

            var list = (from r in db.Racks
                        select new Data.DTO.Rack
                        {
                            Activo = r.Activo,
                            IdRack = r.IdRack,
                            IdSucursal = r.IdSucursal,
                            Lado = r.Lado,
                            Nivel = r.Nivel,
                            Observacion = r.Observacion,
                            Pasillo = r.Pasillo,
                            Posicion = r.Posicion,
                            Zona = r.Zona
                        });

            return list.Where(func).ToList();
        }
    }
}
