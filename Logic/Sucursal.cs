﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Sucursal
    {
        public static List<Data.DTO.Sucursal> Find(HAMEContext db, string IdSucursal)
        {
            Func<Data.DTO.Sucursal, bool> func = d => 1 == 1;

            if (!string.IsNullOrEmpty(IdSucursal))
            {
                func = s => s.IdSucursal.Equals(IdSucursal);
            }

            var list = (from d in db.Sucursals
                        select new Data.DTO.Sucursal
                        {
                            Activa = d.Activa,
                            Direccion = d.Direccion,
                            IdEmpresa = d.IdEmpresa,
                            IdSucursal = d.IdSucursal,
                            Nombre = d.Nombre,
                            Tipo = d.Tipo
                        });

            return list.Where(func).ToList();
        }
    }
}
