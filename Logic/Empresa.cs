﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Empresa
    {
        public static List<Data.DTO.Empresa> Find(HAMEContext context)
        {
            return context.Empresas.Select(d => new Data.DTO.Empresa
            {
                Descripcion = d.Descripcion,
                IdEmpresa = d.IdEmpresa,
                Nombre = d.Nombre,
            }).ToList();
        }
    }
}
