using Data.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductosController : ControllerBase
    {
        private readonly HAMEContext context;

        public ProductosController(HAMEContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Data.DTO.Producto> GetProductosAll(string? IdProducto, string? Nombre, string? Descripcion)
        {
            return Logic.Productos.Find(context, IdProducto, Nombre, Descripcion);
        }

        [HttpPost]
        public IActionResult PostCreateProducto(Data.DTO.Producto producto)
        {
            var result = Logic.Productos.Create(context, producto);
            if (result == null)
                return BadRequest(new { Producto = new { } });
            return Ok(result);
        }

        [HttpPut]
        public IActionResult PutUpdateProducto(Data.DTO.Producto producto)
        {
            var result = Logic.Productos.Update(context, producto);

            if (result == null)
                return BadRequest(new { Producto = new { } });
            return Ok(result);
        }

        [HttpDelete]
        public IActionResult DeleteDisableProducto(string IdMaestro)
        {
            var result = Logic.Productos.Delete(context, IdMaestro);
            if (result == null)
                return BadRequest();
            return Ok(result);
        }
    }
}