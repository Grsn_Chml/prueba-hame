﻿using Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresasController : ControllerBase
    {
        private readonly HAMEContext context;
        public EmpresasController(HAMEContext context) 
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Data.DTO.Empresa> GetEmpresas()
        {
            return Logic.Empresa.Find(context);
        }
    }
}
