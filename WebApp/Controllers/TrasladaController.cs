﻿using Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrasladaController : ControllerBase
    {
        private readonly HAMEContext context;

        public TrasladaController(HAMEContext context)
        {
            this.context = context;
        }

        [HttpPut]
        public IActionResult PutUpdateProducto(Data.DTO.Producto producto)
        {
            var result = Logic.Traslada.Update(context, producto);

            if (result == null)
                return BadRequest(new { Producto = new { } });
            return Ok(result);
        }
    }
}
