﻿using Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SucursalesController : ControllerBase
    {
        private readonly HAMEContext context;

        public SucursalesController(HAMEContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Data.DTO.Sucursal> GetSucursales(string? IdSucursal)
        {
            return Logic.Sucursal.Find(context, IdSucursal);
        }
    }
}
