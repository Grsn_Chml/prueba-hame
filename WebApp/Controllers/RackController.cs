﻿using Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RackController : ControllerBase
    {
        private readonly HAMEContext context;

        public RackController(HAMEContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Data.DTO.Rack> GetRack(string? IdRack, string? IdSucursal)
        {
            return Logic.Rack.Find(context, IdRack, IdSucursal);
        }
    }
}
