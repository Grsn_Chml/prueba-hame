﻿using Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PosicionesController : ControllerBase
    {
        private readonly HAMEContext context;

        public PosicionesController(HAMEContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<Data.DTO.Posicion> GetPosiciones(string? IdPosicion, string? IdRack)
        {
            return Logic.Posicion.Get(context, IdPosicion, IdRack);
        }

        [HttpPost]
        public IActionResult PostCreatePosicion(Data.DTO.Posicion posicion)
        {
            var result = Logic.Posicion.Create(context, posicion);
            if (result == null)
                return BadRequest(new { Posicion = new { } });
            return Ok(result);
        }
    }
}
