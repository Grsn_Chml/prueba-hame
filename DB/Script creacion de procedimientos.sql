CREATE OR ALTER PROCEDURE CreateProducto 
	@IdProducto varchar(75),
	@IdEmpresa varchar(75),
	@IdMaestro varchar(75),
	@IdInventario varchar(75),
	@IdPosicion varchar(75),
	@IdKardex varchar(75),
	@Codigo varchar(150),
	@CodigoFabricante varchar(75),
	@Nombre varchar(250),
	@Descripcion varchar(500),
	@SKU varchar(25),
	@Unidad varchar(25),
	@Unidades int,
	@Costo decimal(18,2),
	@Precio int,
	@Precio1 decimal(18,2),
	@Precio2 decimal(18,2),
	@Precio3 decimal(18,2),
	@Precio4 decimal(18,2),
	@Precio5 decimal(18,2),
	@Imagen varchar(1000),
	@Existencia decimal(18,2),
	@Minimo int,
	@Maximo int,
	@Tipo varchar(25)
AS
	BEGIN TRANSACTION [ProductosTransaction]
		BEGIN TRY
			-- INSERTAMOS EN MAESTRO
			INSERT INTO Maestro(IdMaestro, IdEmpresa, Codigo, CodigoFabricante, Nombre, Descripcion, Tipo, Costo, Minimo, Maximo, Imagen, Habilitado, SKU) VALUES (
				@IdMaestro, @IdEmpresa, @Codigo, @CodigoFabricante, @Nombre, @Descripcion, @Tipo, @Costo, @Minimo, @Maximo, @Imagen, 1, @SKU
			);

			-- INSERTAMOS EN PRODUCTO
			INSERT INTO Producto (IdProducto, IdEmpresa, IdMaestro, Codigo, CodigoFabricante, Nombre, Descripcion, SKU, Unidad, Unidades, Costo, Precio, Precio1, Precio2, Precio3, Precio4, Precio5, Imagen, Habilitado) VALUES (
				@IdProducto, @IdEmpresa, @IdMaestro, @Codigo, @CodigoFabricante, @Nombre, @Descripcion, @SKU, @Unidad, @Unidades, @Costo, @Precio, @Precio1, @Precio2, @Precio3, @Precio4, @Precio5, @Imagen, 1
			);

			-- INSERTAMOS EN INVENTARIO
			INSERT INTO Inventario (IdInventario, IdPosicion, IdMaestro, Existencia, Minimo, Maximo, Fecha) VALUES (
				@IdInventario, @IdPosicion, @IdMaestro, @Existencia, @Minimo, @Maximo, GETDATE()
			);

			-- INSERTAMOS EN KARDEX
			INSERT INTO Kardex (IdKardex, IdInventario, IdProducto, Cantidad, Existencia, Fecha, Accion, Motivo, Codigo, SKU, Nombre) VALUES (
				@IdKardex, @IdInventario, @IdProducto, @Existencia, 0, GETDATE(), 'CREAR', 'Se crea el nuevo producto',  @Codigo, @SKU, @Nombre
			);
			COMMIT TRANSACTION [ProductosTransaction]
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION [ProductosTransaction]
		END CATCH
GO

CREATE OR ALTER PROCEDURE UpdateProducto
	@IdProducto varchar(75),
	@IdEmpresa varchar(75),
	@IdMaestro varchar(75),
	@IdInventario varchar(75),
	@IdPosicion varchar(75),
	@IdKardex varchar(75),
	@Codigo varchar(150),
	@CodigoFabricante varchar(75),
	@Nombre varchar(250),
	@Descripcion varchar(500),
	@SKU varchar(25),
	@Unidad varchar(25),
	@Unidades int,
	@Costo decimal(18,2),
	@Precio int,
	@Precio1 decimal(18,2),
	@Precio2 decimal(18,2),
	@Precio3 decimal(18,2),
	@Precio4 decimal(18,2),
	@Precio5 decimal(18,2),
	@Imagen varchar(1000),
	@Existencia decimal(18,2),
	@Minimo int,
	@Maximo int,
	@Tipo varchar(25)
AS
	BEGIN TRANSACTION [ProductosTransaction]
		BEGIN TRY
			-- ACTUALIZAMOS MAESTRO
			UPDATE 
				Maestro 
			SET
				Codigo = @Codigo, CodigoFabricante = @CodigoFabricante, Nombre = @Nombre, Descripcion = @Descripcion, Tipo = @Tipo, Costo = @Costo,
				Minimo = @Minimo, Imagen = @Imagen, Maximo = @Maximo, SKU = @SKU
			WHERE 
				IdMaestro = @IdMaestro AND IdEmpresa = @IdEmpresa;

			-- ACTUALIZAMOS PRODUCTO
			UPDATE
				Producto
			SET
				Codigo = @Codigo, CodigoFabricante = @CodigoFabricante, Nombre = @Nombre, Descripcion = @Descripcion, SKU = @SKU, Unidad = @Unidad,
				Unidades = @Unidades, Costo = @Costo, Precio = @Precio, Precio1 = @Precio1, Precio2 = @Precio2, Precio3 = @Precio3, Precio4 = @Precio4,
				Precio5 = @Precio5, Imagen = @Imagen
			WHERE
				IdProducto = @IdProducto AND IdEmpresa = @IdEmpresa AND IdMaestro = @IdMaestro;

			-- ACTUALIZAMOS INVENTARIO
			UPDATE
				Inventario
			SET
				Existencia = @Existencia, Minimo = @Minimo, Maximo = @Maximo, IdPosicion = @IdPosicion
			WHERE
				IdInventario = @IdInventario AND IdMaestro = @IdMaestro;

			-- INSERTAMOS EN KARDEX
			INSERT INTO Kardex (IdKardex, IdInventario, IdProducto, Cantidad, Existencia, Fecha, Accion, Motivo, Codigo, SKU, Nombre) VALUES (
				@IdKardex, @IdInventario, @IdProducto, @Existencia, 0, GETDATE(), 'ACTUALIZAR', 'Se se actualiza el producto',  @Codigo, @SKU, @Nombre
			);

			COMMIT TRANSACTION [ProductosTransaction]
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION [ProductosTransaction]
		END CATCH
GO

CREATE OR ALTER PROCEDURE DeleteProducto
	@IdMaestro varchar(75)
AS
	BEGIN TRANSACTION [ProductosTransaction]
		BEGIN TRY
			-- SE DESACTIVA EL MAESTRO
			UPDATE 
				Maestro
			SET
				Habilitado = 0
			WHERE
				IdMaestro = @IdMaestro;

			-- SE DESACTIVA EL PRODUCTO
			UPDATE
				Producto
			SET
				Habilitado = 0
			WHERE
				IdMaestro = @IdMaestro;

			DECLARE @Codigo VARCHAR(75), @SKU VARCHAR(75), @Nombre VARCHAR(250), @IdProducto VARCHAR(75), @IdInventario VARCHAR(75);

			SELECT @Codigo = Codigo, @SKU = SKU, @Nombre = Nombre FROM Maestro WHERE IdMaestro = @IdMaestro;
			SELECT @IdProducto = IdProducto FROM Producto WHERE IdMaestro = @IdMaestro;
			SELECT @IdInventario = IdInventario FROM Inventario WHERE IdMaestro = @IdMaestro;

			INSERT INTO Kardex (IdInventario, IdKardex, IdProducto, Fecha, Accion, Motivo, Codigo, SKU, Nombre) VALUES (
				@IdInventario, NEWID (), @IdProducto,  GETDATE(), 'ELIMINAR', 'Se se desactiva el producto',  @Codigo, @SKU, @Nombre
			);

			COMMIT TRANSACTION [ProductosTransaction]
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION [ProductosTransaction]
		END CATCH
GO

CREATE OR ALTER PROCEDURE CreatePosicion
	@IdRack varchar(75),
	@IdPosicion varchar(75),
	@Codigo varchar(30),
	@Nivel int,
	@Posicion int,
	@Fondo int,
	@Zona varchar(25),
	@Observaciones varchar(25)
AS
	BEGIN TRANSACTION [ProductosTransaction]
		BEGIN TRY
			-- INSERTAMOS EN POSICION
			INSERT INTO Posicion(IdRack, IdPosicion, Codigo, Nivel, Posicion, Fondo, Activo, Zona, Observaciones) VALUES (
				@IdRack, @IdPosicion, @Codigo, @Nivel, @Posicion, @Fondo, 1, @Zona, @Observaciones
			);

			COMMIT TRANSACTION [ProductosTransaction]
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION [ProductosTransaction]
		END CATCH
GO

CREATE OR ALTER PROCEDURE UpdateTraslado
	@IdProducto varchar(75),
	@IdEmpresa varchar(75),
	@IdMaestro varchar(75),
	@IdInventario varchar(75),
	@IdPosicion varchar(75),
	@Codigo varchar(150),
	@Nombre varchar(250),
	@SKU varchar(25),
	@Existencia decimal(18,2)
AS
	BEGIN TRANSACTION [ProductosTransaction]
		BEGIN TRY
			-- ACTUALIZAMOS MAESTRO
			UPDATE 
				Maestro 
			SET
				IdEmpresa = @IdEmpresa
			WHERE 
				IdMaestro = @IdMaestro;

			-- ACTUALIZAMOS PRODUCTO
			UPDATE
				Producto
			SET
				IdEmpresa = @IdEmpresa
			WHERE
				IdProducto = @IdProducto AND IdMaestro = @IdMaestro;

			-- ACTUALIZAMOS INVENTARIO
			UPDATE
				Inventario
			SET
				IdPosicion = @IdPosicion
			WHERE
				IdInventario = @IdInventario AND IdMaestro = @IdMaestro;

			-- INSERTAMOS EN KARDEX
			INSERT INTO Kardex (IdKardex, IdTraslado, IdProducto, Cantidad, Existencia, Fecha, Accion, Motivo, Codigo, SKU, Nombre) VALUES (
				NEWID (), NEWID (), @IdProducto, @Existencia, 0, GETDATE(), 'TRASLADAR', 'Se traslada el producto',  @Codigo, @SKU, @Nombre
			);

			COMMIT TRANSACTION [ProductosTransaction]
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION [ProductosTransaction]
		END CATCH