USE HAME;

DROP TABLE IF EXISTS Empresa;
CREATE TABLE Empresa (
	IdEmpresa varchar(75) primary key,
	Nombre varchar(150),
	Descripcion varchar(500),
	Creada DateTime,
	Modificada DateTime,
	Activa bit
);

DROP TABLE IF EXISTS Sucursal;
CREATE TABLE Sucursal(
	IdEmpresa varchar(75),
	IdSucursal varchar(75) primary key,
	Nombre varchar(150),
	Direccion varchar(500),
	Creada DateTime,
	Modificada DateTime,
	Activa bit,
	Tipo varchar(20)
);
ALTER TABLE Sucursal ADD CONSTRAINT FK_IdEmpresa FOREIGN KEY (IdEmpresa) REFERENCES Empresa(IdEmpresa) ON DELETE CASCADE;

DROP TABLE IF EXISTS Rack;
CREATE TABLE Rack (
	IdRack varchar(75) primary key,
	IdSucursal varchar(75),
	Nivel int,
	Posicion varchar(15),
	Zona varchar(25),
	Activo bit,
	Observacion varchar(250),
	Lado varchar(2),
	Pasillo VARCHAR (2)
);
ALTER TABLE Rack ADD CONSTRAINT FK_IdSucursal FOREIGN KEY (IdSucursal) REFERENCES Sucursal(IdSucursal) ON DELETE CASCADE;

DROP TABLE IF EXISTS Posicion;
CREATE TABLE Posicion (
	IdRack varchar(75),
	IdPosicion varchar(75) primary key,
	Codigo varchar(30),
	Nivel int,
	Posicion int,
	Fondo int, 
	Activo bit,
	Zona varchar(25),
	Observaciones varchar(250)
);
ALTER TABLE Posicion ADD CONSTRAINT FK_IdRack FOREIGN KEY (IdRack) REFERENCES Rack(IdRack) ON DELETE CASCADE;

DROP TABLE IF EXISTS Maestro;
CREATE TABLE Maestro (
	IdMaestro varchar(75) primary key,
	IdEmpresa varchar(75),
	Numero int identity(1,1),
	Codigo varchar(75),
	CodigoFabricante varchar(75) NULL,
	Nombre varchar(250),
	Descripcion varchar(500) NULL,
	Tipo varchar(25),
	Costo decimal(18,2),
	Minimo int,
	Maximo int,
	Imagen varchar(1000) null,
	Habilitado bit,
	SKU varchar(25)
);
ALTER TABLE Maestro ADD CONSTRAINT FK_IdEmpresaMaestro FOREIGN KEY (IdEmpresa) REFERENCES Empresa(IdEmpresa) ON DELETE CASCADE;

DROP TABLE IF EXISTS Producto;
CREATE TABLE Producto (
	IdProducto varchar(75) primary key,
	IdEmpresa varchar(75),
	IdMaestro varchar(75),
	Numero int identity(1,1),
	Codigo varchar(150),
	CodigoFabricante varchar(75) NULL,
	Nombre varchar(250),
	Descripcion varchar(500) NULL,
	SKU varchar(25),
	Unidad varchar(25),
	Unidades int,
	Costo decimal(18,2),
	Precio int,
	Precio1 decimal(18,2),
	Precio2 decimal(18,2),
	Precio3 decimal(18,2),
	Precio4 decimal(18,2),
	Precio5 decimal(18,2),
	Imagen varchar(1000) null,
	Habilitado bit
);

ALTER TABLE Producto ADD CONSTRAINT FK_IdEmpresaProducto FOREIGN KEY (IdEmpresa) REFERENCES Empresa(IdEmpresa) ON DELETE CASCADE;
ALTER TABLE Producto ADD CONSTRAINT FK_IdMaestroProducto FOREIGN KEY (IdMaestro) REFERENCES Maestro(IdMaestro);

DROP TABLE IF EXISTS Inventario;
CREATE TABLE Inventario (
	IdInventario varchar(75) primary key,
	IdPosicion varchar(75),
	IdMaestro varchar(75),
	Existencia decimal(18,2),
	Minimo int,
	Maximo int,
	Fecha DateTime
);

ALTER TABLE Inventario ADD CONSTRAINT FK_IdPosicionInventario FOREIGN KEY (IdPosicion) REFERENCES Posicion(IdPosicion);
ALTER TABLE Inventario ADD CONSTRAINT FK_IdMaestroInventario FOREIGN KEY (IdMaestro) REFERENCES Maestro(IdMaestro);

DROP TABLE IF EXISTS Kardex;
CREATE TABLE Kardex (
	IdKardex varchar(75) primary key,
	IdVenta varchar(75) null,
	IdCompra varchar(75) null,
	IdTraslado varchar(75) null,
	IdInventario varchar(75) null,
	IdProducto varchar(75) null,
	Cantidad decimal(18,2),
	Existencia decimal(18,2),
	Fecha DateTime,
	Accion varchar(150),
	Motivo varchar(300),
	Codigo varchar(75),
	SKU varchar(75),
	Nombre varchar(300),
);

ALTER TABLE Kardex ADD CONSTRAINT FK_IdInventarioKardex FOREIGN KEY (IdInventario) REFERENCES Inventario(IdInventario);
ALTER TABLE Kardex ADD CONSTRAINT FK_IdProductoKardex FOREIGN KEY (IdProducto) REFERENCES Producto(IdProducto);