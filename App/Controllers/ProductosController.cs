﻿using App.Models;
using App.Serices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace App.Controllers
{
    public class ProductosController : Controller
    {
        Helper helper;

        public ProductosController(IOptions<Config> config)
        {
            helper = new Helper();
            helper.Url = config.Value.ApiUrl;
        }

        public IActionResult Index()
        {
            var result = helper.GetProductos();
            return View(result);
        }

        public IActionResult Add() 
        {
            ViewBag.Empresas = helper.GetEmpresas();
            ViewBag.Posiciones = helper.GetPosiciones();
            ViewBag.Tipos = helper.GetTipos();
            return View(); 
        }

        [HttpPost]
        public async Task<IActionResult> Add(Producto producto)
        {
            ViewBag.Empresas = helper.GetEmpresas();
            ViewBag.Posiciones = helper.GetPosiciones();
            ViewBag.Tipos = helper.GetTipos();

            if (!ModelState.IsValid)
            {
                return View();
            }
            var result = await helper.PostProducto(producto);

            return View(result);
        }

        public IActionResult Edit(string id)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("../");
            }

            var resutl = helper.GetProductosById(id);
            if(resutl.Count == 1)
            {
                ViewBag.Empresas = helper.GetEmpresas();
                ViewBag.Posiciones = helper.GetPosiciones();
                ViewBag.Tipos = helper.GetTipos();

                return View(resutl[0]);
            }
            else
                return Redirect("../");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Producto producto)
        {
            ViewBag.Empresas = helper.GetEmpresas();
            ViewBag.Posiciones = helper.GetPosiciones();
            ViewBag.Tipos = helper.GetTipos();

            if (!ModelState.IsValid)
            {
                return View();
            }

            var result = await helper.PutProducto(producto);

            return View(result);
        }

        public IActionResult Delete(string id)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("~/Productos");
            }

            var resutl = helper.GetProductosById(id);
            if (resutl.Count == 1)
            {
                ViewBag.Empresas = helper.GetEmpresas();
                ViewBag.Posiciones = helper.GetPosiciones();
                ViewBag.Tipos = helper.GetTipos();

                return View(resutl[0]);
            }
            else
                return Redirect("~/Productos");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Producto producto)
        {
            if (string.IsNullOrWhiteSpace(producto.IdMaestro))
            {
                return View();
            }

            var resutl = helper.DeleteProducto(producto);

            return Redirect("~/Productos");
        }
    }
}
