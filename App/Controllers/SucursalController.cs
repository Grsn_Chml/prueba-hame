﻿using App.Models;
using App.Serices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace App.Controllers
{
    public class SucursalController : Controller
    {
        Helper helper;

        public SucursalController(IOptions<Config> config)
        {
            helper = new Helper();
            helper.Url = config.Value.ApiUrl;
        }

        public IActionResult Index()
        {
            return View(helper.GetSucursales());
        }
    }
}
