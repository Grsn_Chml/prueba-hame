﻿using App.Models;
using App.Serices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace App.Controllers
{
    public class PosicionesController : Controller
    {
        Helper helper;

        public PosicionesController(IOptions<Config> config)
        {
            helper = new Helper();
            helper.Url = config.Value.ApiUrl;
        }

        public IActionResult Index()
        {
            return View(helper.GetPosiciones());
        }

        public IActionResult Add()
        {
            ViewBag.racks = helper.GetRacks();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(Posicion posicion) {
            ViewBag.racks = helper.GetRacks();
            if (!ModelState.IsValid)
            {
                return View();
            }

            var result = await helper.PostPosicion(posicion);
            return View(result);
        }
    }
}
