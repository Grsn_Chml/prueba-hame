﻿using App.Models;
using App.Serices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace App.Controllers
{
    public class TrasladoController : Controller
    {
        Helper helper;

        public TrasladoController(IOptions<Config> config) {
            helper = new Helper();
            helper.Url = config.Value.ApiUrl;
        }

        public IActionResult Index()
        {
            var result = helper.GetProductos();
            return View(result);
        }

        public IActionResult Traslada(string id)
        {
            var result = helper.GetProductosById(id);
            if (result.Count == 1)
            {
                var product = result[0];

                var positions = helper.GetPosicionById(product.IdPosicion);

                if(positions.Count == 1)
                {
                    var position = positions[0];

                    var racks = helper.GetRackById(position.IdRack);

                    if(racks.Count == 1)
                    {
                        var rack = racks[0];

                        var sucursales = helper.GetSucursalById(rack.IdSucursal);

                        if(sucursales.Count == 1)
                        {
                            var sucursal = sucursales[0];

                            product.IdRack = rack.IdRack;
                            product.Rack = rack.Zona;

                            product.Sucursal = sucursal.Nombre;
                            product.IdSucursal = sucursal.IdSucursal;

                            ViewBag.Empresas = helper.GetEmpresas();
                            ViewBag.Sucursales = helper.GetSucursales();
                            ViewBag.Racks = new List<Rack>();
                            ViewBag.Posiciones = new List<Posicion>();

                            return View(product);
                        }
                        else
                        {
                            return Redirect("~/Traslado");
                        }
                    }
                    else
                    {
                        return Redirect("~/Traslado");
                    }
                }
                else
                {
                    return Redirect("~/Traslado");
                }
            }
            else
            {
                return Redirect("~/Traslado");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Traslada(Producto producto)
        {
            ViewBag.Empresas = helper.GetEmpresas();
            ViewBag.Sucursales = helper.GetSucursales();
            ViewBag.Racks = new List<Rack>();
            ViewBag.Posiciones = new List<Posicion>();

            if (string.IsNullOrEmpty(producto.IdPosicionNew) || string.IsNullOrEmpty(producto.IdRackNew) || string.IsNullOrEmpty(producto.IdSucursalNew))
            {
                ModelState.AddModelError("name", "Debe seleccionar la Sucursal, Rack y Posicion");
                return View(producto);
            }

            producto.IdSucursal = producto.IdSucursalNew;
            producto.IdPosicion = producto.IdPosicionNew;
            producto.IdRack = producto.IdRackNew;


            var result = await helper.PutTraslado(producto);

            return Redirect("~/Traslado");
        }

        [HttpGet]
        public IActionResult GetRack(string id)
        {
            var racks = helper.GetRackBySucursal(id);

            return Ok(racks);
        }

        [HttpGet]
        public IActionResult GetPosition(string id)
        {
            var racks = helper.GetPosicionByRack(id);

            return Ok(racks);
        }
    }
}
