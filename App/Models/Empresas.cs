﻿namespace App.Models
{
    public class Empresas
    {
        public string IdEmpresa { get; set; } = null!;
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
    }
}
