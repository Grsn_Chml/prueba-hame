﻿namespace App.Models
{
    public class Tipo
    {
        public string? IdTipo { get; set; }
        public string? Nombre { get; set; }
    }
}
