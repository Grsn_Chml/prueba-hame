﻿using System.ComponentModel.DataAnnotations;

namespace App.Models
{
    public class Producto
    {
        public string? IdProducto { get; set; }
        [Required(ErrorMessage = "Empresa es necesaria")]
        public string? IdEmpresa { get; set; }
        public string? IdMaestro { get; set; }
        [Required(ErrorMessage = "Posicion es necesaria")]
        public string? IdPosicion { get; set; }
        public string? IdInventario { get; set; }
        public string? IdKardex { get; set; }
        public int? Numero { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public string? Codigo { get; set; }
        public string? CodigoFabricante { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public string? Sku { get; set; }
        [Display(Name = "Presentacion")]
        [Required(ErrorMessage = "{0} es requerido")]
        public string? Unidad { get; set; }
        [Display(Name = "Unidad")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? Unidades { get; set; }
        [Range(0, 9999.00)]
        public decimal? Costo { get; set; }
        [Display(Name = "Precio por Defecto")]
        [Required(ErrorMessage = "{0} es requerido")]
        public int? Precio { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public decimal? Precio1 { get; set; }
        public decimal? Precio2 { get; set; }
        public decimal? Precio3 { get; set; }
        public decimal? Precio4 { get; set; }
        public decimal? Precio5 { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public string? Tipo { get; set; }
        public string? Imagen { get; set; }
        public bool? Habilitado { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public decimal? Existencia { get; set; }
        public string? Posicion { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public int? Minimo { get; set; }
        [Required(ErrorMessage = "{0} es requerido")]
        public int? Maximo { get; set; }

        public string? IdRack { get; set; }
        public string? Rack { get; set; }
        public string? Sucursal { get; set; }
        public string? IdSucursal { get; set; }

        public string? IdSucursalNew { get; set; }
        public string? IdRackNew { get; set; }
        public string? IdPosicionNew { get; set; }
    }
}
