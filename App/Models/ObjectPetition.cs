﻿namespace App.Models
{
    public class ObjectPetition<T>
    {
        public string? Code { get; set; }
        public string? Message { get; set; }
        public object? Result { get; set; }
    }
}
