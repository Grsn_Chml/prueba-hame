﻿using System.ComponentModel.DataAnnotations;

namespace App.Models
{
    public class Posicion
    {
        public string? IdRack { get; set; }
        public string? IdPosicion { get; set; }
        public string? Codigo { get; set; }
        public int? Nivel { get; set; }
        [Display(Name = "Posicion")]
        public int? Posicion1 { get; set; }
        public int? Fondo { get; set; }
        public bool? Activo { get; set; }
        public string? Zona { get; set; }
        public string? Observaciones { get; set; }
        public string? Rack { get; set; }
        public string? Sucursal { get; set; }
    }
}
