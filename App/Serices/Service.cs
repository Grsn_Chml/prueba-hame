﻿using System.Text.Json.Nodes;
using System.Text;
using Newtonsoft.Json;

namespace App.Serices
{
    public class Service
    {
        private static readonly HttpClient client = new HttpClient();

        public static string GetPetition(Dictionary<string, string> parametros, string url)
        {
            try
            {
                var webRequest = System.Net.WebRequest.Create(url);
                string response = string.Empty;

                if (webRequest != null)
                {
                    webRequest.Method = "GET";
                    webRequest.Timeout = 60000;
                    webRequest.ContentType = "application/json";

                    foreach (var parametro in parametros)
                    {
                        webRequest.Headers.Add(parametro.Key, parametro.Value);
                    }

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            response = sr.ReadToEnd();
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static async Task<string> PostPetition(object objeto, string url)
        {
            string responseString;
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(objeto), Encoding.UTF8, "application/json");

                var response = await client.PostAsync(url, content);

                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                responseString = ex.Message;
            }

            return responseString;
        }

        public static async Task<string> PutPetition(object objeto, string url)
        {
            string responseString;
            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(objeto), Encoding.UTF8, "application/json");

                var response = await client.PutAsync(url, content);

                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                responseString = ex.Message;
            }

            return responseString;
        }

        public static async Task<string> DeletePetition(string url)
        {
            string responseString;
            try
            {
                var response = await client.DeleteAsync(url);

                responseString = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                responseString = ex.Message;
            }

            return responseString;
        }
    }
}
