﻿using App.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace App.Serices
{
    public class Helper
    {
        public string Url { get; set; }

        #region GET PETITIONS
        public List<Producto> GetProductos()
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/productos");
            return JsonConvert.DeserializeObject<List<Producto>>(response);
        }

        public List<Producto> GetProductosById(string IdProducto)
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/productos?IdProducto=" + IdProducto);
            return JsonConvert.DeserializeObject<List<Producto>>(response);
        }

        public List<Empresas> GetEmpresas()
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/empresas");
            return JsonConvert.DeserializeObject<List<Empresas>>(response);
        }

        public List<Posicion> GetPosiciones()
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/posiciones");
            return JsonConvert.DeserializeObject<List<Posicion>>(response);
        }

        public List<Posicion> GetPosicionById(string IdPosicion)
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/posiciones?IdPosicion=" + IdPosicion);
            return JsonConvert.DeserializeObject<List<Posicion>>(response);
        }

        public List<Posicion> GetPosicionByRack(string IdRack)
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/posiciones?IdRack=" + IdRack);
            return JsonConvert.DeserializeObject<List<Posicion>>(response);
        }

        public List<Tipo> GetTipos()
        {
            return new List<Tipo>() { new Tipo { IdTipo = "SERVICIO", Nombre = "Servicio" }, new Tipo { IdTipo = "PRODUCTO", Nombre = "Producto" } };
        }

        public List<Rack> GetRacks()
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/rack");
            return JsonConvert.DeserializeObject<List<Rack>>(response);
        }

        public List<Rack> GetRackById(string IdRack)
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/rack?IdRack=" + IdRack);
            return JsonConvert.DeserializeObject<List<Rack>>(response);
        }

        public List<Rack> GetRackBySucursal(string IdSucursal)
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/rack?IdSucursal=" + IdSucursal);
            return JsonConvert.DeserializeObject<List<Rack>>(response);
        }

        public List<Sucursal> GetSucursales() 
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/sucursales");
            return JsonConvert.DeserializeObject<List<Sucursal>>(response);
        }

        public List<Sucursal> GetSucursalById(string IdSucursal)
        {
            string response = Serices.Service.GetPetition(new Dictionary<string, string>(), Url + "api/sucursales?IdSucursal=" + IdSucursal);
            return JsonConvert.DeserializeObject<List<Sucursal>>(response);
        }
        #endregion

        #region POST PETITIONS
        public async Task<Producto> PostProducto(Producto producto)
        {
            string response = await Serices.Service.PostPetition(producto, Url + "api/productos");
            return JsonConvert.DeserializeObject<Producto>(response);
        }

        public async Task<Posicion> PostPosicion(Posicion posicion)
        {
            string response = await Serices.Service.PostPetition(posicion, Url + "api/posiciones");
            return JsonConvert.DeserializeObject<Posicion>(response);
        }
        #endregion

        #region PUT PETITIONS
        public async Task<Producto> PutProducto(Producto producto)
        {
            string response = await Serices.Service.PutPetition(producto, Url + "api/productos");
            return JsonConvert.DeserializeObject<Producto>(response);
        }

        public async Task<Producto> PutTraslado(Producto producto)
        {
            string response = await Serices.Service.PutPetition(producto, Url + "api/traslada");
            return JsonConvert.DeserializeObject<Producto>(response);
        }
        #endregion

        #region DELETE PETICIONS
        public async Task<string> DeleteProducto(Producto producto)
        {
            return await Serices.Service.DeletePetition(string.Format(Url + "api/productos?IdMaestro={0}", producto.IdMaestro));
        }
        #endregion
    }
}
