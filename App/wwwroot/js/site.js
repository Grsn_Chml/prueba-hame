﻿function searchRack() {
    var value = document.getElementById("IdSucursalNew").value;

    var IdRackNewSelect = document.getElementById('IdRackNew');

    fetch('/traslado/GetRack?id=' + value)
        .then((response) => response.json())
        .then(
            (data) => {

                $(IdRackNewSelect).empty();
                $(document.getElementById('IdPosicionNew')).empty();
                $(IdRackNewSelect).append('<option value=""></option>');
                for (var i in data) {

                    let rack = data[i];
                    $(IdRackNewSelect).append('<option value=' + rack.idRack + '>' + rack.lado + '-' + rack.nivel + '-' + rack.pasillo + '-' + rack.posicion + '</option>');
                }
            });

}

function searchPosition() {
    var value = document.getElementById("IdRackNew").value;
    var IdPosicionNew = document.getElementById('IdPosicionNew');
    
    fetch('/traslado/GetPosition?id=' + value)
        .then((response) => response.json())
        .then(
            (data) => {
                $(IdPosicionNew).empty();
                $(IdPosicionNew).append('<option value=""></option>');

                for (var i in data) {

                    let pos = data[i];
                    $(IdPosicionNew).append('<option value=' + pos.idPosicion + '>' + pos.codigo + '</option>');
                }
            });
}